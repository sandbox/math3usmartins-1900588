Summary
---------------
Media Mover SNS (MM SNS) extends the Media Mover (MM) module by integrating it
with Amazon SNS and adding three new actions to MM configurations.

If your site use any of these actions, whenever a file is uploaded, you'll
publish a message on Amazon SNS, in the topic that you defined in the MM
configuration.

Author: Matheus Martins / math3usmartins (http://drupal.org/user/324559)

Installation
---------------
Important: This module requires Media Mover and Libraries.

Configuration
---------------
After installed, MM SNS brings a new configuration page/tab within the Media
Mover settings page.

First you must inform the path to the AWS PHP SDK directory, which must be
placed inside the default library directory.

Then you have to define the list of Amazon SNS topics that you plan to use.

You must inform the Topic ARN, Access Key and Secret Access Key for every topic.

You also have to inform a friendly name for each topic you define.
This is necessary for the next step: Using the MM actions.

Usage
---------------
When you create a new MM configuration, you'll be able to pick the actions
supported by this module.

The actions are available in the MM "storage" and "complete" steps.

MM step: Storage

Publish a message on Amazon SNS
Upload file into Amazon S3 and publish a message on Amazon SNS
Notice that the second action above also integrates with Amazon S3.
ie. The file is copied to S3 and a message in published on SNS.

This is done with help from the MM S3 built-in module.

MM step: Complete

Publish a message on Amazon SNS
Once you select an action provided by this module, you'll see a new field
(select/dropdown) with the list of topics that you defined in the configuration
step.

You just need to pick the SNS Topic that you want to use for this action.

That's all.
